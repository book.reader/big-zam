use crate::{
    concrete_syntax::{
        chars::CharTok, error_tree, fmt_pair, numbers::Number, strings::StringLit,
        symbols::QualName, take_exactly, ConcreteSyntax, Rule,
    },
    result::Result,
};

use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

#[derive(Debug, Clone, PartialEq)]
pub enum Literal {
    Num(Number),
    Str(StringLit),
    Sym(QualName),
    Char(CharTok),
    Unit,
}

impl ConcreteSyntax for Literal {
    const EXPECTED_RULE: Rule = Rule::literal;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let [pair] = take_exactly(pair)?;

        match pair.as_rule() {
            Rule::num_lit => Number::parse_pair(pair).map(Self::Num),
            Rule::str_lit => StringLit::parse_pair(pair).map(Self::Str),
            Rule::qual_name => QualName::parse_pair(pair).map(Self::Sym),
            Rule::char_lit => CharTok::parse_pair(pair).map(Self::Char),
            _ => Err(error_tree(
                "Literal was of unrecognized inner type",
                vec![fmt_pair(&pair)],
            )),
        }
    }
}

impl TreeFormat for Literal {
    fn treeify(&self) -> Tree<String> {
        match self {
            Literal::Num(num) => num.treeify(),
            Literal::Str(string) => string.treeify(),
            Literal::Sym(sym) => sym.treeify(),
            Literal::Char(ch) => Tree::Leaf(ch.to_string()),
            Literal::Unit => Tree::Leaf("()".to_string()),
        }
    }
}

#[cfg(test)]
mod test {
    use pest::Parser;

    use crate::concrete_syntax::{
        chars::Character,
        numbers::{NumBase, NumSeq},
        symbols::Symbol,
        ZamParser,
    };

    use super::*;

    #[test]
    fn char_token() {
        let test_str = "'a'";
        let got = Literal::test_parse(test_str);
        let want = Literal::Char(CharTok(Character::Raw('a')));
        assert_eq!(got, want)
    }

    #[test]
    fn num_token() {
        let test_str = "0x15";
        let got = Literal::test_parse(test_str);
        let want = Literal::Num(Number {
            base: Some(NumBase::Hexadecimal),
            sign: None,
            digits: NumSeq::WholeOnly {
                has_dot: false,
                text: "15".to_string(),
            },
        });
        assert_eq!(got, want)
    }

    #[test]
    fn sym_token() {
        let test_str = "foo.bar.baz";
        let got = Literal::test_parse(test_str);
        let want = Literal::Sym(
            QualName::from_names(vec![
                Symbol::test_parse("foo"),
                Symbol::test_parse("bar"),
                Symbol::test_parse("baz"),
            ])
            .expect("Failed to generate test expectation"),
        );

        assert_eq!(got, want)
    }

    #[test]
    fn str_token() {
        let test_str = "\"Just another string\"";
        let got = Literal::test_parse(test_str);
        let want = Literal::Str(StringLit::test_parse(test_str));
        assert_eq!(got, want)
    }
}
