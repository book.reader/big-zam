use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{drop_next, expr::Expr, maybe_drop_next, ConcreteSyntax, Rule},
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub struct If {
    pub cond: Expr,
    pub true_branch: Expr,
    pub false_branch: Option<Expr>,
}

impl ConcreteSyntax for If {
    const EXPECTED_RULE: Rule = Rule::if_expr;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();

        drop_next(&mut pairs, Rule::IF_KW)?;
        let cond = Expr::parse_next("No Condition", &mut pairs)?;
        drop_next(&mut pairs, Rule::THEN_KW)?;
        let true_branch = Expr::parse_next("No True Branch", &mut pairs)?;
        let false_branch = if maybe_drop_next(&mut pairs, Rule::ELSE_KW)? {
            Some(Expr::parse_next("Missing False Branch", &mut pairs)?)
        } else {
            None
        };

        Ok(If {
            cond,
            true_branch,
            false_branch,
        })
    }
}

impl TreeFormat for If {
    fn treeify(&self) -> Tree<String> {
        let cond = self
            .cond
            .treeify()
            .map_head(&|head| format!("Condition: {}", head));

        let true_branch = self
            .true_branch
            .treeify()
            .map_head(&|head| format!("True Branch: {}", head));

        let mut children = vec![cond, true_branch];
        if let Some(false_branch) = &self.false_branch {
            children.push(
                false_branch
                    .treeify()
                    .map_head(&|head| format!("False Branch: {}", head)),
            );
        }

        Tree::Branch("If".to_string(), children)
    }
}

#[cfg(test)]
mod test {
    use crate::concrete_syntax::symbols::Symbol;

    use super::*;

    #[test]
    fn no_else() {
        let got = If::test_parse("if foo then bar");

        let want = If {
            cond: Symbol::test_parse("foo").into(),
            true_branch: Symbol::test_parse("bar").into(),
            false_branch: None,
        };

        assert_eq!(got, want)
    }

    #[test]
    fn simple() {
        let got = If::test_parse("if foo then bar else baz");

        let want = If {
            cond: Symbol::test_parse("foo").into(),
            true_branch: Symbol::test_parse("bar").into(),
            false_branch: Some(Symbol::test_parse("baz").into()),
        };

        assert_eq!(got, want)
    }

    #[test]
    fn complex() {
        let got = If::test_parse("if (45 + val) then \"Orange\" else 5");

        let want = If {
            cond: Expr::test_parse("(45 + val)"),
            true_branch: Expr::test_parse("\"Orange\""),
            false_branch: Some(Expr::test_parse("5")),
        };

        assert_eq!(got, want)
    }
}
