use itertools::Itertools;
use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{
        error, error_tree,
        literals::Literal,
        sequences::{List, Tuple},
        ConcreteSyntax, Rule,
    },
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub enum Pattern {
    Literal(Literal),
    Tuple(Tuple),
    List(List),
    Juxtaposed(Vec<Self>),
}

impl ConcreteSyntax for Pattern {
    const EXPECTED_RULE: Rule = Rule::pattern;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let (mut patterns, errs): (Vec<_>, Vec<_>) = pair
            .into_inner()
            .map(|pair| match pair.as_rule() {
                Rule::literal => Literal::parse_pair(pair).map(Self::Literal),
                Rule::tuple => Tuple::parse_pair(pair).map(Self::Tuple),
                Rule::list => List::parse_pair(pair).map(Self::List),
                rule => unreachable!(
                    "Should not be able to get rule {:?} within {:?}",
                    rule,
                    Self::EXPECTED_RULE
                ),
            })
            .partition_result();

        if errs.len() > 0 {
            return Err(error_tree("Parsing errors in pattern", errs));
        }

        match patterns.len() {
            0 => unreachable!("Should not be able to read no patterns from rule"),
            1 => Ok(patterns
                .pop()
                .ok_or_else(|| error("Missing pattern after size check"))?),
            _ => Ok(Self::Juxtaposed(patterns)),
        }
    }
}

impl TreeFormat for Pattern {
    fn treeify(&self) -> Tree<String> {
        match self {
            Pattern::Literal(lit) => lit.treeify().swap_head("Literal Pattern".to_string()).1,
            Pattern::Tuple(tup) => tup.treeify().swap_head("Tuple pattern".to_string()).1,
            Pattern::List(lst) => lst.treeify().swap_head("List pattern".to_string()).1,
            Pattern::Juxtaposed(inner) => Tree::Branch(
                "Juxtaposed".to_string(),
                inner.into_iter().map(|pat| pat.treeify()).collect(),
            ),
        }
    }
}

impl From<Literal> for Pattern {
    fn from(value: Literal) -> Self {
        Self::Literal(value)
    }
}
impl From<List> for Pattern {
    fn from(value: List) -> Self {
        Self::List(value)
    }
}
impl From<Tuple> for Pattern {
    fn from(value: Tuple) -> Self {
        Self::Tuple(value)
    }
}
impl From<Vec<Self>> for Pattern {
    fn from(value: Vec<Self>) -> Self {
        Self::Juxtaposed(value)
    }
}

#[cfg(test)]
mod test {
    use crate::concrete_syntax::{
        expr::Expr,
        literals::Literal,
        pattern::Pattern,
        sequences::{CommaExprSeq, Tuple},
        ConcreteSyntax,
    };

    #[test]
    fn literal_sym() {
        let test_str = "func";
        let got = Pattern::test_parse(test_str);
        let want = Literal::test_parse(test_str).into();

        assert_eq!(got, want)
    }

    #[test]
    fn literal_list() {
        let test_str = "[a, b, c]";
        let got = Pattern::test_parse(test_str);
        let want = {
            let literals = vec!["a", "b", "c"]
                .into_iter()
                .map(|s| Literal::test_parse(s))
                .collect::<Vec<_>>();

            let seq = CommaExprSeq::from(literals);
            Pattern::List(seq.into())
        };

        assert_eq!(got, want)
    }

    #[test]
    fn literal_tuple() {
        let test_str = "(a, b)";
        let got = Pattern::test_parse(test_str);
        let want = {
            let literals = vec!["a", "b"]
                .into_iter()
                .map(|s| Expr::test_parse(s))
                .collect::<Vec<_>>();
            Pattern::from(Tuple::from(literals))
        };

        assert_eq!(got, want)
    }

    #[test]
    fn literal_juxtaposed() {
        let test_str = "func arg1 arg2";
        let got = Pattern::test_parse(test_str);
        let want = {
            let literals = vec!["func", "arg1", "arg2"]
                .into_iter()
                .map(|s| Pattern::test_parse(s))
                .collect();

            Pattern::Juxtaposed(literals)
        };

        assert_eq!(got, want)
    }
}
