use std::{fmt::Display, iter};

use itertools::{Either, Itertools};
use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{error, ConcreteSyntax, Rule},
    result::Result,
};

#[derive(PartialEq, Eq, Debug, Clone, Hash, PartialOrd, Ord)]
pub struct Symbol(pub String);

impl Symbol {
    pub fn as_ref(&self) -> &str {
        &self.0
    }
}

impl ConcreteSyntax for Symbol {
    const EXPECTED_RULE: Rule = Rule::legal_symbol;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        Ok(Symbol(pair.as_str().to_string()))
    }
}

impl Display for Symbol {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl TreeFormat for Symbol {
    fn treeify(&self) -> Tree<String> {
        Tree::Leaf(self.to_string())
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
/// A `qualified.name` in the language. Parameterized over the type being used
/// to represent names in the tree. Basically a non-empty list.
/// Ordered as `qualifier[0].qualifier[1].<etc>.bound_name`
pub struct QualName {
    bound_name: Symbol,
    qualifier: Vec<Symbol>,
}

impl ConcreteSyntax for QualName {
    const EXPECTED_RULE: Rule = Rule::qual_name;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let symbols = pair
            .into_inner()
            .map(|symbol| Symbol::parse_pair(symbol))
            .collect::<Result<Vec<_>>>()?;
        QualName::from_names(symbols)
            .ok_or_else(|| error("Qualified name must have at least one name element"))
    }
}

impl QualName {
    pub fn symbol(bound_name: Symbol) -> Self {
        QualName {
            bound_name,
            qualifier: vec![],
        }
    }

    /// Empty lists of names are not valid symbols, so return None.
    pub fn from_names(mut names: Vec<Symbol>) -> Option<Self> {
        if names.is_empty() {
            None
        } else {
            let bound_name = names.remove(0);
            Some(QualName {
                bound_name,
                qualifier: names,
            })
        }
    }

    /// Convert this name (if it has no `.`s in it) to the Symbol alone.
    pub fn try_as_symbol(self) -> Either<Symbol, Self> {
        if self.qualifier.is_empty() {
            Either::Left(self.bound_name)
        } else {
            Either::Right(self)
        }
    }

    pub fn as_symbol_ref(&self) -> Option<&Symbol> {
        if self.qualifier.is_empty() {
            Some(&self.bound_name)
        } else {
            None
        }
    }

    #[cfg(test)]
    pub fn test_new<S: AsRef<str>, I: IntoIterator<Item = S>>(head: S, qualifiers: I) -> Self {
        QualName {
            bound_name: Symbol::test_parse(head.as_ref()),
            qualifier: qualifiers
                .into_iter()
                .map(|q| Symbol::test_parse(q.as_ref()))
                .collect(),
        }
    }
}

impl From<Symbol> for QualName {
    fn from(val: Symbol) -> Self {
        Self::symbol(val)
    }
}

impl TreeFormat for QualName {
    fn treeify(&self) -> Tree<String> {
        let names = iter::once(&self.bound_name)
            .chain(self.qualifier.iter())
            .map(|s| s.as_ref())
            .join(".");

        Tree::Leaf(format!("Qualified Name( {names} )"))
    }
}

impl Display for QualName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        self.qualifier
            .iter()
            .fold(Ok(()), |acc, q| acc.and(write!(f, "{}.", q)))
            .and(write!(f, "{}", self.bound_name))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn single_component() {
        let name = "foo";
        let got = QualName::test_parse(name);
        let want = QualName::test_new(name, vec![]);
        assert_eq!(got, want)
    }

    #[test]
    fn multi_component() {
        let symbol = "foo.bar.baz";
        let got = QualName::test_parse(symbol);
        let want = QualName::test_new("foo", vec!["bar", "baz"]);
        assert_eq!(got, want)
    }

    #[test]
    fn name_with_dashes() {
        let test_str = "->";
        let got = QualName::test_parse(test_str);
        let want = QualName::test_new(test_str, vec![]);
        assert_eq!(got, want)
    }
}
