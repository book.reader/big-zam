use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};
use std::fmt::Debug;

use crate::{
    concrete_syntax::{
        comments::Comment, drop_next, error_tree, expr::Expr, fmt_pair,
        statements::binding_pattern::BindingPattern, take_exactly, ConcreteSyntax, Rule,
    },
    result::Result,
};

pub mod binding_pattern;

#[derive(Debug, Clone, PartialEq)]
pub enum InnerStmt {
    LetStmt(BindingPattern),
    ArgStmt(BindingPattern),
    ExprSemicolon(Expr),
}

impl ConcreteSyntax for InnerStmt {
    const EXPECTED_RULE: Rule = Rule::inner_stmt;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let [pair] = take_exactly(pair)?;
        let rule = pair.as_rule();
        let mut pairs = pair.into_inner();

        match rule {
            Rule::let_stmt => {
                drop_next(&mut pairs, Rule::LET_KW)?;
                let binding = BindingPattern::parse_next("Missing Let Patter", &mut pairs)?;
                drop_next(&mut pairs, Rule::SEMICOLON)?;

                Ok(InnerStmt::LetStmt(binding))
            }
            Rule::arg_stmt => {
                drop_next(&mut pairs, Rule::LET_KW)?;
                let binding = BindingPattern::parse_next("Missing Let Patter", &mut pairs)?;
                drop_next(&mut pairs, Rule::SEMICOLON)?;

                Ok(InnerStmt::ArgStmt(binding))
            }
            Rule::expr_semicolon => {
                let expr = Expr::parse_next("Missing Statement Expr", &mut pairs)?;
                Ok(InnerStmt::ExprSemicolon(expr))
            }
            wat => unreachable!("Impossible rule encountered {:?}", wat),
        }
    }
}

impl TreeFormat for InnerStmt {
    fn treeify(&self) -> Tree<String> {
        match self {
            InnerStmt::LetStmt(binder) => {
                Tree::Branch("Let Statement".to_string(), vec![binder.treeify()])
            }
            InnerStmt::ArgStmt(binder) => {
                Tree::Branch("Arg Statement".to_string(), vec![binder.treeify()])
            }
            InnerStmt::ExprSemicolon(inner_expr) => {
                Tree::Branch("Expression".to_string(), vec![inner_expr.treeify()])
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Stmt {
    pre_comment: Option<Comment>,
    post_comment: Option<Comment>,
    inner_stmt: InnerStmt,
}

impl Stmt {
    pub fn to_inner(self) -> InnerStmt {
        self.inner_stmt
    }
}

impl ConcreteSyntax for Stmt {
    const EXPECTED_RULE: Rule = Rule::stmt;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let [pair] = take_exactly(pair)?;

        match pair.as_rule() {
            Rule::both_comment_stmt => {
                let [pre_comment, inner, post_comment] = take_exactly(pair)?;
                let pre_comment = Comment::parse_pair(pre_comment)?;
                let post_comment = Comment::parse_pair(post_comment)?;
                let inner_stmt = InnerStmt::parse_pair(inner)?;
                Ok(Stmt {
                    pre_comment: Some(pre_comment),
                    post_comment: Some(post_comment),
                    inner_stmt,
                })
            }
            Rule::pre_comment_stmt => {
                let [pre_comment, inner] = take_exactly(pair)?;
                let pre_comment = Comment::parse_pair(pre_comment)?;
                let inner_stmt = InnerStmt::parse_pair(inner)?;
                Ok(Stmt {
                    pre_comment: Some(pre_comment),
                    post_comment: None,
                    inner_stmt,
                })
            }
            Rule::post_comment_stmt => {
                let [inner, post_comment] = take_exactly(pair)?;
                let post_comment = Comment::parse_pair(post_comment)?;
                let inner_stmt = InnerStmt::parse_pair(inner)?;
                Ok(Stmt {
                    pre_comment: None,
                    post_comment: Some(post_comment),
                    inner_stmt,
                })
            }
            Rule::inner_stmt => {
                let inner_stmt = InnerStmt::parse_pair(pair)?;
                Ok(Stmt {
                    pre_comment: None,
                    post_comment: None,
                    inner_stmt,
                })
            }
            rule => Err(error_tree(
                format!(
                    "Unrecognized rule {:?} parsed inside commented statement:",
                    rule
                ),
                vec![fmt_pair(&pair)],
            )),
        }
    }
}

impl From<InnerStmt> for Stmt {
    fn from(inner_stmt: InnerStmt) -> Self {
        Stmt {
            pre_comment: None,
            post_comment: None,
            inner_stmt,
        }
    }
}

impl TreeFormat for Stmt {
    fn treeify(&self) -> Tree<String> {
        let Self {
            pre_comment,
            post_comment,
            inner_stmt,
        } = self;

        Tree::Branch(
            "Statement".to_string(),
            vec![
                pre_comment
                    .as_ref()
                    .map(|c| format!("Pre comment: {c}").into()),
                Some(inner_stmt.treeify()),
                post_comment
                    .as_ref()
                    .map(|c| format!("Post comment: {c}").into()),
            ]
            .into_iter()
            .filter_map(|i| i)
            .collect(),
        )
    }
}

#[cfg(test)]
mod test {

    use pest::Parser;

    use crate::concrete_syntax::{
        branches::if_branch::If, expr::InnerExpr, pattern::Pattern, ZamParser,
    };

    use super::*;

    #[test]
    fn let_sym_eq_if_expr() {
        let input_string = "let baz = if (43 + 5) then 9 else \"Orange\";";
        let stmt = Stmt::parse_pair(
            ZamParser::parse(Rule::stmt, input_string)
                .expect("Failed to parse test string")
                .next()
                .unwrap(),
        )
        .expect("Failed to parse stmt pairs");

        let expected = InnerStmt::LetStmt(BindingPattern {
            value: InnerExpr::If(Box::new(If::test_parse(
                "if (43 + 5) then 9 else \"Orange\"",
            )))
            .into(),
            pattern: Pattern::test_parse("baz"),
            type_annotation: None,
        })
        .into();

        assert_eq!(stmt, expected)
    }
}
