use itertools::Itertools;
use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{
        drop_next, drop_next_back, error, error_tree,
        expr::{Expr, InnerExpr},
        literals::Literal,
        statements::Stmt,
        ConcreteSyntax, Rule,
    },
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub struct CommaExprSeq(pub Vec<Expr>, bool);

impl CommaExprSeq {
    fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
}

impl ConcreteSyntax for CommaExprSeq {
    const EXPECTED_RULE: Rule = Rule::comma_expr_seq;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        pair.into_inner()
            .try_fold((Vec::new(), false), |(mut exprs, _), pair| {
                match pair.as_rule() {
                    Rule::expr => {
                        exprs.push(Expr::parse_pair(pair)?);
                        Ok((exprs, false))
                    }
                    Rule::SEQ_SEP => Ok((exprs, true)),
                    rule => Err(format!(
                        "Unknown Rule::{:?} found parsing comma expr sequence",
                        rule
                    )
                    .into()),
                }
            })
            .map(|(exprs, tailing)| Self(exprs, tailing))
    }
}

impl TreeFormat for CommaExprSeq {
    fn treeify(&self) -> Tree<String> {
        Tree::Branch(
            "Comma-Expression sequence".into(),
            self.0.iter().map(|ch| ch.treeify()).collect(),
        )
    }
}

impl Default for CommaExprSeq {
    fn default() -> Self {
        Self(Vec::with_capacity(0), false)
    }
}

impl<E: Into<Expr>> From<Vec<E>> for CommaExprSeq {
    fn from(value: Vec<E>) -> Self {
        Self(value.into_iter().map(Into::into).collect(), false)
    }
}

impl IntoIterator for CommaExprSeq {
    type Item = Expr;
    type IntoIter = std::vec::IntoIter<Expr>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Tuple(pub CommaExprSeq);

impl Tuple {
    pub fn collapse(self) -> InnerExpr {
        if self.0.is_empty() {
            InnerExpr::Lit(Literal::Unit)
        } else {
            InnerExpr::Tuple(self)
        }
    }

    pub fn exprs(self) -> Vec<Expr> {
        self.0 .0
    }
}

impl ConcreteSyntax for Tuple {
    const EXPECTED_RULE: Rule = Rule::tuple;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();
        drop_next(&mut pairs, Rule::LPAREN)?;
        drop_next_back(&mut pairs, Rule::RPAREN)?;
        let seq = CommaExprSeq::maybe_parse_next(&mut pairs)?.unwrap_or_default();
        Ok(Self(seq))
    }
}

impl TreeFormat for Tuple {
    fn treeify(&self) -> Tree<String> {
        self.0.treeify().swap_head("Tuple".into()).1
    }
}

impl From<Vec<Expr>> for Tuple {
    fn from(value: Vec<Expr>) -> Self {
        Self(CommaExprSeq(value, false))
    }
}
#[derive(Debug, Clone, PartialEq)]
pub struct List(pub CommaExprSeq);

impl List {
    pub fn exprs(self) -> Vec<Expr> {
        self.0 .0
    }
}

impl ConcreteSyntax for List {
    const EXPECTED_RULE: Rule = Rule::list;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();
        drop_next(&mut pairs, Rule::LBRACK)?;
        drop_next_back(&mut pairs, Rule::RBRACK)?;

        Ok(Self(CommaExprSeq::parse_next(
            "Missing sequence in list",
            &mut pairs,
        )?))
    }
}

impl TreeFormat for List {
    fn treeify(&self) -> Tree<String> {
        self.0.treeify().swap_head("List".into()).1
    }
}

impl From<CommaExprSeq> for List {
    fn from(value: CommaExprSeq) -> Self {
        Self(value)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Block {
    pub stmts: Vec<Stmt>,
    pub final_expr: Option<Expr>,
}

impl ConcreteSyntax for Block {
    const EXPECTED_RULE: Rule = Rule::block;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();
        drop_next(&mut pairs, Rule::LCURLY)?;
        drop_next_back(&mut pairs, Rule::RCURLY)?;

        let (stmts, mut errs): (Vec<_>, Vec<_>) = pairs
            .take_while_ref(|pair| pair.as_rule() == Rule::stmt)
            .map(Stmt::parse_pair)
            .partition_result();

        let final_expr = {
            let maybe_next = pairs.at_most_one().map_err(error)?;
            match maybe_next {
                Some(next) => match Expr::parse_pair(next) {
                    Ok(expr) => Some(expr),
                    Err(e) => {
                        errs.push(e);
                        None
                    }
                },
                None => None,
            }
        };

        if errs.len() > 0 {
            return Err(error_tree("Got errors parsing block:", errs));
        }

        Ok(Self { stmts, final_expr })
    }
}

impl From<Expr> for Block {
    fn from(expr: Expr) -> Self {
        Self {
            stmts: Vec::with_capacity(0),
            final_expr: Some(expr),
        }
    }
}

impl TreeFormat for Block {
    fn treeify(&self) -> Tree<String> {
        let head = "Block Expression".to_owned();
        let expr = if let Some(ref res_expr) = self.final_expr {
            Tree::leaf(format!("Result?: {}", res_expr.treeify()))
        } else {
            Tree::leaf("No final expression.".to_owned())
        };
        let stmt_tree = Tree::Branch(
            "Statements".to_string(),
            self.stmts.iter().map(|stmt| stmt.treeify()).collect(),
        );
        Tree::Branch(head, vec![stmt_tree, expr])
    }
}
