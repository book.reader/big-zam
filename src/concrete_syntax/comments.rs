use std::fmt::Display;

use crate::{
    concrete_syntax::{ConcreteSyntax, Rule},
    result::Result,
};
use itertools::Itertools;
use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

#[derive(Debug, Clone, PartialEq)]
pub struct Comment(Vec<String>);

impl ConcreteSyntax for Comment {
    const EXPECTED_RULE: Rule = Rule::comment;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        Ok(Self(
            pair.as_str()
                // For each line...
                .split("\n")
                // There shouldn't be any empty lines before clipping.
                .filter(|line| *line != "")
                // Clip off leading spaces and then '#'
                .map(|line| line.trim_start()[1..].to_owned())
                // Then stick the comments back together
                .collect(),
        ))
    }
}

impl Display for Comment {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0.iter().join("\n"))
    }
}

impl TreeFormat for Comment {
    fn treeify(&self) -> Tree<String> {
        Tree::Branch(
            "Comment".to_string(),
            self.0.iter().cloned().map(Tree::Leaf).collect(),
        )
    }
}

#[cfg(test)]
mod test {
    use pest::Parser;

    use crate::concrete_syntax::ZamParser;

    use super::*;

    fn parse_comment(test_str: &str) -> Comment {
        let pair = ZamParser::parse(Rule::comment, test_str)
            .expect("Failed to parse comment")
            .next()
            .expect("Failed to parse comment pair");

        Comment::parse_pair(pair).expect("Failed to convert pair to comment")
    }

    #[test]
    fn comment_newline() {
        let test_str = "# blargh\n";
        let got = parse_comment(test_str);
        let want = Comment(vec![test_str[1..test_str.len() - 1].to_string()]);
        assert_eq!(got, want);
    }

    #[test]
    fn comment_eoi() {
        let test_str = "# blargh";
        let got = parse_comment(test_str);
        let want = Comment(vec![test_str[1..].to_string()]);
        assert_eq!(got, want);
    }

    #[test]
    fn comment_lead_spaces() {
        let test_str = "    # blargh";
        let got = parse_comment(test_str);
        let want = Comment(vec![test_str[5..].to_string()]);
        assert_eq!(got, want)
    }

    #[test]
    fn comment_multiline() {
        let test_str = "  # foo\n   # bar\n";
        let got = parse_comment(test_str);
        let want = Comment(vec![" foo".to_string(), " bar".to_string()]);
        assert_eq!(got, want)
    }
}
