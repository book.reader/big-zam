use std::{convert::TryFrom, fmt::Display, ops::Deref};

use itertools::Itertools;
use pest::iterators::Pair;

use crate::{
    concrete_syntax::{
        drop_next, drop_next_back, error, error_tree, fmt_pair, ConcreteSyntax, Rule,
    },
    result::Result,
};

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
pub enum Character {
    Unicode(char), // this value is stored as a unicode val directly
    Escaped(char), // a correct escape is stored as the value it resolves to
    Raw(char),     // This is just a char with no escape
    Invalid(char), // A character that's not a recognized escape sequence.
}

impl ConcreteSyntax for Character {
    const EXPECTED_RULE: Rule = Rule::char_val;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let spanned_char = pair.as_str();

        let pair = match pair.into_inner().exactly_one() {
            Err(_) => {
                return spanned_char
                    .chars()
                    .exactly_one()
                    .map(Self::Raw)
                    .map_err(|e| {
                        format!(
                            "Raw char spanned incorrect number of characters, wanted one: `{e}`"
                        )
                        .into()
                    })
            }
            Ok(pair) => pair,
        };

        match pair.as_rule() {
            Rule::unicode_escape_seq => {
                let mut pairs = pair.into_inner();
                drop_next(&mut pairs, Rule::STRING_ESCAPE)?;
                drop_next(&mut pairs, Rule::UNICODE_SIGIL)?;
                drop_next(&mut pairs, Rule::LCURLY)?;
                drop_next_back(&mut pairs, Rule::RCURLY)?;

                let hex_str = pairs
                    .exactly_one()
                    .map_err(|e| format!("No hex pair:\n{e}"))?
                    .as_str();

                let num = match u32::from_str_radix(hex_str, 16) {
                    Ok(v) => v,
                    Err(e) => {
                        return Err(format!(
                            "sequence `{hex_str}` failed to parse as hexadecimal. Parse error: {e}",
                        )
                        .into())
                    }
                };

                match char::try_from(num) {
                        Ok(v) => Ok(Self::Unicode(v)),
                        Err(e) => Err(format!(
                            "failed to convert hext `{hex_str}` (decimal {num}) to a Unicode character. Error: {e}"
                        ).into()),
                    }
            }
            // Or a basic escape sequence.
            Rule::basic_escape_seq => {
                let any_char = &pair.as_str()[1..]; // Slice off escape slash.

                match any_char {
                    // Basic escapes.
                    "t" => Ok(Character::Escaped('\t')),
                    "n" => Ok(Character::Escaped('\n')),
                    "r" => Ok(Character::Escaped('\r')),
                    // Weird legacy shit.
                    "f" => Ok(Character::Escaped('\u{C}')),
                    "b" => Ok(Character::Escaped('\u{8}')),
                    "v" => Ok(Character::Escaped('\u{B}')),
                    // Character-sequence-terminating characters that can be escaped.
                    "`" => Ok(Character::Escaped('`')),
                    "\"" => Ok(Character::Escaped('"')),
                    "\'" => Ok(Character::Escaped('\'')),
                    ch => {
                        if ch.len() == 1 {
                            ch.chars()
                  .nth(0)
                  .ok_or_else(|| {
                    error("Expected to have a character in the successful character parse?")
                  })
                  .map(Character::Invalid)
                        } else {
                            Err(format!(
                "Got an successful parse for an invalid escape character, but the parsed sequence was not of length one. Actual: <{:#?}>", ch
              ).into())
                        }
                    }
                }
            }
            _ => Err(error_tree(
                "Got unrecognized pair in Character parser",
                vec![fmt_pair(&pair)],
            )),
        }
    }
}

impl Deref for Character {
    type Target = char;

    fn deref(&self) -> &Self::Target {
        match self {
            Character::Unicode(c)
            | Character::Escaped(c)
            | Character::Raw(c)
            | Character::Invalid(c) => c,
        }
    }
}

impl From<&Character> for char {
    fn from(ch: &Character) -> char {
        **ch
    }
}

// TODO: Merge this into the Token enum and delete this?
// Can't implement `Parseable` with &'static str context if so. Maybe?
#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct CharTok(pub Character);

impl ConcreteSyntax for CharTok {
    const EXPECTED_RULE: Rule = Rule::char_lit;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();
        drop_next(&mut pairs, Rule::SQUOTE)?;
        let ch = Character::parse_next("Missing inner character in token", &mut pairs)?;
        drop_next(&mut pairs, Rule::SQUOTE)?;

        Ok(Self(ch))
    }
}

impl CharTok {
    // Convert `self` to a string that would be tokenized as `self`.
    pub fn to_string(&self) -> String {
        match self.0 {
            Character::Unicode(u) => u.escape_unicode().to_string(),
            Character::Escaped(c) => format!("\\{}", c),
            Character::Raw(c) => format!("{}", c),
            Character::Invalid(c) => format!("{}", c),
        }
    }
}

impl Display for CharTok {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            Character::Unicode(c) => write!(f, "Unicode: {}", c.escape_default()),
            Character::Escaped(c) => write!(f, "Escaped: {}", c.escape_default()),
            Character::Raw(c) => write!(f, "Raw: {}", c.escape_default()),
            Character::Invalid(c) => write!(f, "Invalid: {}", c.escape_default()),
        }
    }
}

#[cfg(test)]
mod test {

    use pest::Parser;

    use crate::concrete_syntax::ZamParser;

    use super::*;

    fn parse_char_val(input: &str) -> CharTok {
        let mut pairs = ZamParser::parse(Rule::char_lit, input)
            .unwrap_or_else(|e| panic!("Failed to parse test input: {}\nErr: {}", input, e));

        CharTok::parse_pair(pairs.next().expect("Test string yielded no pairs."))
            .unwrap_or_else(|e| panic!("Failed to parse test pairs of {}\nErr: {}", input, e))
    }

    #[test]
    fn raw() {
        let test_str = "'a'";
        let want = CharTok(Character::Raw('a'));
        let got = parse_char_val(test_str);
        assert_eq!(got, want);
    }

    #[test]
    fn escaped() {
        let test_str = r#"'\t'"#; // A raw rust string containing 2 chars.
        let want = CharTok(Character::Escaped('\t'));
        let got = parse_char_val(test_str);
        assert_eq!(got, want);
    }

    #[test]
    fn invalid() {
        let test_str = r#"'\q'"#; // A raw rust string containing 2 chars.

        let want = CharTok(Character::Invalid('q'));
        let got = parse_char_val(test_str);
        assert_eq!(got, want);
    }

    #[test]
    fn unicode() {
        let test_str = r#"'\u{1F60A}'"#;

        let want = CharTok(Character::Unicode('😊'));
        let got = parse_char_val(test_str);
        assert_eq!(got, want);
    }
}
