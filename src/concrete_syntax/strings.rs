use std::{fmt::Display, iter::FromIterator};

use itertools::Itertools;
use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{
        chars::Character, drop_next, drop_next_back, error_tree, expr::Expr, take_exactly,
        ConcreteSyntax, Rule,
    },
    result::Result,
};

impl ConcreteSyntax for Vec<Character> {
    const EXPECTED_RULE: Rule = Rule::literal_chunk;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let (chunk, errs): (_, Vec<_>) = pair
            .into_inner()
            .map(|p| Character::parse_pair(p))
            .partition_result();

        if errs.len() > 0 {
            Err(error_tree("Got errors parsing literal chunk:", errs))
        } else {
            Ok(chunk)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum StringChunk {
    Plain(Vec<Character>),
    Interp(Expr),
}

impl ConcreteSyntax for StringChunk {
    const EXPECTED_RULE: Rule = Rule::str_chunk;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let [pair] = take_exactly(pair)?;
        let rule = pair.as_rule();
        let mut pairs = pair.into_inner();
        match rule {
            Rule::interpolated_chunk => {
                drop_next(&mut pairs, Rule::GRAVE)?;
                drop_next_back(&mut pairs, Rule::GRAVE)?;
                let expr = Expr::parse_next("Missing interpolated expression", &mut pairs)?;
                Ok(Self::Interp(expr))
            }
            Rule::literal_chunk => {
                let (chars, errs): (_, Vec<_>) =
                    pairs.map(Character::parse_pair).partition_result();

                if errs.len() > 0 {
                    return Err(error_tree("Got errors parsing characters", errs));
                }
                Ok(Self::Plain(chars))
            }
            rule => Err(format!("Got unknown Rule::{:?} parsing string chunk", rule).into()),
        }
    }
}

impl Display for StringChunk {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            StringChunk::Plain(chars) => {
                write!(
                    f,
                    "{}",
                    chars.iter().map(Into::<char>::into).collect::<String>()
                )
            }
            StringChunk::Interp(expr) => {
                write!(f, "`{}`", expr.treeify())
            }
        }
    }
}

impl FromIterator<Character> for StringChunk {
    fn from_iter<T: IntoIterator<Item = Character>>(iter: T) -> Self {
        Self::Plain(iter.into_iter().collect())
    }
}

impl From<Vec<Character>> for StringChunk {
    fn from(val: Vec<Character>) -> Self {
        Self::Plain(val)
    }
}

impl From<Expr> for StringChunk {
    fn from(expr: Expr) -> Self {
        StringChunk::Interp(expr)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct StringLit(Vec<StringChunk>);

impl FromIterator<StringChunk> for StringLit {
    fn from_iter<T: IntoIterator<Item = StringChunk>>(iter: T) -> Self {
        Self(iter.into_iter().collect())
    }
}

impl IntoIterator for StringLit {
    type Item = StringChunk;
    type IntoIter = std::vec::IntoIter<StringChunk>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl From<Vec<StringChunk>> for StringLit {
    fn from(val: Vec<StringChunk>) -> Self {
        Self(val)
    }
}

impl Display for StringLit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\"")?;
        for chunk in &self.0 {
            chunk.fmt(f)?;
        }
        write!(f, "\"")
    }
}

impl ConcreteSyntax for StringLit {
    const EXPECTED_RULE: Rule = Rule::str_lit;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();
        drop_next(&mut pairs, Rule::DQUOTE)?;
        drop_next_back(&mut pairs, Rule::DQUOTE)?;

        let (chunks, errs): (_, Vec<_>) = pairs.map(StringChunk::parse_pair).partition_result();

        if errs.len() > 0 {
            return Err(error_tree("Got errors parsing string chunks", errs));
        }

        Ok(Self(chunks))
    }
}

impl TreeFormat for StringLit {
    fn treeify(&self) -> Tree<String> {
        Tree::Branch(
            "String Literal".to_string(),
            self.0
                .iter()
                .map(|chunk| match chunk {
                    StringChunk::Plain(chunk) => Tree::Leaf(format!(
                        "Plain Chunk: {}",
                        chunk.iter().map(Into::<char>::into).collect::<String>()
                    )),
                    StringChunk::Interp(chunk) => {
                        Tree::Branch("Interpolated Chunk".to_string(), vec![chunk.treeify()])
                    }
                })
                .collect(),
        )
    }
}

#[cfg(test)]
mod test {

    use pest::Parser;

    use crate::concrete_syntax::ZamParser;

    use super::*;

    #[test]
    fn empty_str() {
        let test_str = "\"\"";
        let got = StringLit::test_parse(test_str);
        let want = StringLit(vec![]);

        assert_eq!(got, want)
    }

    #[test]
    fn plain_str() {
        let test_str = "\"Just a normal String\"";
        let want = StringLit(vec![test_str[1..(test_str.len() - 1)]
            // This is how we clip double-quotes off.
            .chars()
            .map(Character::Raw)
            .collect()]);

        let got = StringLit::test_parse(test_str);

        assert_eq!(got, want);
    }

    #[test]
    fn interpolated_str() {
        let test_str = "\"`2 + 3`\"";
        let want = StringLit(vec![StringChunk::Interp(
            Expr::parse_pair(
                ZamParser::parse(Rule::expr, &test_str[2..test_str.len() - 2])
                    .unwrap()
                    .next()
                    .unwrap(),
            )
            .unwrap(),
        )]);

        let got = StringLit::test_parse(test_str);

        assert_eq!(got, want);
    }

    #[test]
    fn mixed_str() {
        let first_str = "\"Some words with the number ";
        let expr_str = "`2 + 3`";
        let expr_val = {
            let pair = ZamParser::parse(Rule::expr, &expr_str[1..expr_str.len() - 1])
                .expect("Failed to parse interpolated test string")
                .next()
                .expect("Failed to get nested expr pair");

            Expr::parse_pair(pair).expect("Failed to parse test expression")
        };
        let last_str = " interpolated into them.\"";

        let want = StringLit(vec![
            first_str[1..].chars().map(Character::Raw).collect(),
            expr_val.into(),
            last_str[..last_str.len() - 1]
                .chars()
                .map(Character::Raw)
                .collect(),
        ]);

        let got = StringLit::test_parse(&format!("{first_str}{expr_str}{last_str}"));

        assert_eq!(got, want);
    }
}
