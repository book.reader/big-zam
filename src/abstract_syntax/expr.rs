use std::iter;

use rosary::{format::TreeFormat, vec_tree::Tree, RoseTree};

use crate::{
    abstract_syntax::{
        literal::Literal, op::PrecedenceGraph, pattern::Pattern, AbstractSyntax, Stmt,
    },
    concrete_syntax::{
        branches::if_branch::If,
        error_tree,
        expr::{Expr as ConcreteExpr, InnerExpr},
        lambda::Lambda,
        sequences::Block,
        symbols::{QualName, Symbol},
    },
    result::{self, Result},
};

#[derive(Debug, Clone, PartialEq)]
pub enum Expr<Id> {
    Literal(Literal<Id>),
    List(Vec<Self>),
    Tuple(Vec<Self>),
    Block {
        body: Vec<Stmt<Id>>,
        ret_val: Option<Box<Self>>,
    },
    Lambda {
        params: Vec<Pattern<Id>>,
        body: Box<Self>,
    },
    Appl {
        head: Box<Self>,
        args: Vec<Self>,
    },
    If {
        cond: Box<Self>,
        true_branch: Box<Self>,
        false_branch: Option<Box<Self>>,
    },
    Match {
        matched_value: Box<Self>,
        arms: Vec<(Pattern<Id>, Self)>,
    },
}

impl<Id> Expr<Id> {
    /// If this expression is a symbol, get that symbol.
    pub fn as_id_ref(&self) -> Option<&Id> {
        if let Expr::Literal(lit) = self {
            lit.as_id_ref()
        } else {
            None
        }
    }
}

impl AbstractSyntax<ConcreteExpr> for Expr<Symbol> {
    fn from_concrete(source: ConcreteExpr) -> Result<Self> {
        let source = source.to_inner();
        match source {
            InnerExpr::If(if_expr) => {
                let If {
                    cond,
                    true_branch,
                    false_branch,
                } = *if_expr;
                let cond = Box::new(Expr::from_concrete(cond)?);
                let true_branch = Box::new(Expr::from_concrete(true_branch)?);
                let false_branch = match false_branch.map(Expr::from_concrete) {
                    Some(res) => Some(Box::new(res?)),
                    None => None,
                };
                Ok(Self::If {
                    cond,
                    true_branch,
                    false_branch,
                })
            }
            InnerExpr::Match(match_expr) => {
                let (matched_value, arms) = match_expr.into_inner();
                let matched_value = Box::new(Expr::from_concrete(matched_value)?);
                result::list(
                    "Failed parsing match arms",
                    arms.into_iter().map(|(pattern, expr)| {
                        match (Pattern::from_concrete(pattern), Expr::from_concrete(expr)) {
                            (Ok(pat), Ok(expr)) => Ok((pat, expr)),
                            (Ok(pat), Err(e)) => {
                                Err(error_tree("Expr Failure", vec![pat.treeify(), e]))
                            }
                            (Err(e), Ok(expr)) => {
                                Err(error_tree("Pattern Failure", vec![e, expr.treeify()]))
                            }
                            (Err(pat_err), Err(expr_err)) => {
                                Err(error_tree("Both Failed", vec![pat_err, expr_err]))
                            }
                        }
                    }),
                    &|(p, e): (Pattern<_>, Expr<_>)| {
                        Tree::Branch("Arm".to_string(), vec![p.treeify(), e.treeify()])
                    },
                )
                .map(|arms| Self::Match {
                    matched_value,
                    arms,
                })
            }
            InnerExpr::Block(block) => {
                let Block { stmts, final_expr } = *block;
                let body = result::list(
                    "Errors in Block Body",
                    stmts.into_iter().map(Stmt::from_concrete),
                    |s| s.treeify(),
                );

                let ret_val = final_expr
                    .map(Self::from_concrete)
                    .transpose()
                    .map(|o| o.map(Box::new));

                match (body, ret_val) {
                    (Ok(body), Ok(ret_val)) => Ok(Self::Block { body, ret_val }),
                    (Ok(body), Err(err)) => Err(error_tree(
                        "Error in final block expr",
                        body.into_iter()
                            .map(|s| s.treeify())
                            .chain(iter::once(err))
                            .collect(),
                    )),
                    (Err(errs), Ok(Some(v))) => {
                        let (head, mut child_errs) = errs.destructure();
                        child_errs.push(v.treeify());
                        Err(Tree::Branch(head, child_errs))
                    }
                    (Err(errs), Ok(None)) => {
                        Err(errs.map_head(&|h| format!("Errors in block body {h}")))
                    }
                    (Err(body_errs), Err(ret_val_err)) => Err(error_tree(
                        "Errors in block body and return value.",
                        vec![body_errs, ret_val_err],
                    )),
                }
            }
            InnerExpr::Lit(lit) => Literal::from_concrete(lit).map(Self::Literal),
            InnerExpr::Tuple(tuple) => result::list(
                "Failed to parse tuple sub-expressions",
                tuple.exprs().into_iter().map(Self::from_concrete),
                |e| e.treeify(),
            )
            .map(Self::Tuple),
            InnerExpr::List(list) => result::list(
                "Failed to parse list sub-expressions",
                list.exprs().into_iter().map(Self::from_concrete),
                |e| e.treeify(),
            )
            .map(Self::List),
            InnerExpr::Juxtaposed(juxt) => result::list(
                "Failed to parse individual operator sequence expressions to abstract expressions",
                juxt.into_iter().map(Self::from_concrete),
                |e| e.treeify(),
            )
            .and_then(|juxt| PrecedenceGraph::default().parse_expr(juxt.as_slice())),

            InnerExpr::Lambda(lambda) => {
                let Lambda { args, body } = *lambda;
                let args_result = result::list(
                    "Failed to parse args to lambda expression",
                    args.into_iter().map(Pattern::from_concrete),
                    |e| e.treeify(),
                );

                match (args_result, Self::from_concrete(body)) {
                    (Ok(params), Ok(body)) => Ok(Self::Lambda {
                        body: Box::new(body),
                        params,
                    }),
                    (Ok(_), Err(e)) => Err(Tree::Branch(
                        "Failed to parse lambda body".to_owned(),
                        vec![e],
                    )),
                    (Err(e), Ok(_)) => Err(Tree::Branch(
                        "Failed to parse lambda args".to_owned(),
                        vec![e],
                    )),
                    (Err(ae), Err(ee)) => Err(Tree::Branch(
                        "Failed to parse lambda args and body".to_owned(),
                        vec![ae, ee],
                    )),
                }
            }
        }
    }
}

impl<Id: ToString> TreeFormat for Expr<Id> {
    fn treeify(&self) -> Tree<String> {
        match self {
            Expr::Literal(l) => l.treeify(),
            Expr::List(l) => {
                Tree::Branch("List".to_string(), l.iter().map(|e| e.treeify()).collect())
            }
            Expr::Tuple(t) => {
                Tree::Branch("Tuple".to_string(), t.iter().map(|e| e.treeify()).collect())
            }
            Expr::Block { body, ret_val } => {
                let mut subtrees = Vec::new();
                if body.len() > 0 {
                    subtrees.push(Tree::Branch(
                        "Body".to_string(),
                        body.iter().map(|s| s.treeify()).collect(),
                    ));
                }
                if let Some(ret_val) = ret_val {
                    subtrees.push(Tree::Branch(
                        "Final Expr".to_string(),
                        vec![ret_val.treeify()],
                    ))
                }

                if subtrees.len() > 0 {
                    Tree::Branch("Block".to_string(), subtrees)
                } else {
                    Tree::leaf("Empty Block".to_string())
                }
            }
            Expr::Lambda { params, body: def } => Tree::Branch(
                "Lambda".to_string(),
                vec![
                    Tree::Branch(
                        "Params".to_string(),
                        params.iter().map(|e| e.treeify()).collect(),
                    ),
                    Tree::Branch("Definition".to_string(), vec![def.treeify()]),
                ],
            ),
            Expr::Appl { head, args } => Tree::Branch(
                "Application".to_string(),
                vec![
                    Tree::Branch("Applied function".to_string(), vec![head.treeify()]),
                    Tree::Branch(
                        "Args".to_string(),
                        args.iter().map(|e| e.treeify()).collect(),
                    ),
                ],
            ),
            Expr::If {
                cond,
                true_branch,
                false_branch,
            } => {
                let mut subtrees = vec![
                    cond.treeify().map_head(&|h| format!("Condition: {h}")),
                    true_branch
                        .treeify()
                        .map_head(&|t| format!("True Branch: {t}")),
                ];

                if let Some(f) = false_branch {
                    subtrees.push(f.treeify().map_head(&|f| format!("False Branch: {f}")));
                }

                Tree::Branch("If Expr".to_string(), subtrees)
            }
            Expr::Match {
                matched_value,
                arms,
            } => {
                Tree::Branch(
                    "Match Expr".to_string(),
                    iter::once(Tree::Branch(
                        "Discriminant".to_string(),
                        vec![matched_value.treeify()],
                    ))
                    .chain(arms.iter().map(|(p, e)| {
                        Tree::Branch("Arm".to_string(), vec![p.treeify(), e.treeify()])
                    }))
                    .collect(),
                )
            }
        }
    }
}

impl<Id: From<Symbol>> From<Symbol> for Expr<Id> {
    fn from(value: Symbol) -> Self {
        Expr::Literal(Literal::Name(value.into()))
    }
}
