use std::{collections::HashMap, hash::Hash};

use rosary::vec_tree::Tree;

use crate::{
    abstract_syntax::{expr::Expr, module::ZamFile, Stmt},
    result::Result,
};
