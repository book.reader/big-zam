use enum_primitive::Option::{None, Some};
use std::{collections::HashSet, fmt::Display, hash::Hash, rc::Rc};
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Index<T: Eq + Hash>(Rc<T>);

impl<T: Display + Eq + Hash> Display for Index<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0.to_string())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Interner<T: Eq + Hash>(HashSet<Rc<T>>);

impl<T: Eq + Hash> Interner<T> {
    pub fn new() -> Interner<T> {
        Interner(HashSet::new())
    }

    pub fn with_capacity(size: usize) -> Interner<T> {
        Interner(HashSet::with_capacity(size))
    }

    pub fn intern(&mut self, val: T) -> Index<T> {
        let ptr = self.0.iter().find_map(|ptr| {
            if **ptr == val {
                Some(ptr.clone())
            } else {
                None
            }
        });
        match ptr {
            Some(ptr) => Index(ptr),
            None => {
                let val = Rc::new(val);
                self.0.insert(val.clone());
                Index(val)
            }
        }
    }

    pub fn size(&self) -> usize {
        self.0.len()
    }
}
