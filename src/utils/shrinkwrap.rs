use std::collections::VecDeque;

pub struct ShrinkWrapper<T, I: Iterator<Item = T>> {
    iter: I,
}

pub enum ShrinkStep<T, U> {
    Take(U),    // Take the presented value, return the new accumulator
    Pass(T, U), // Pass on the value, taking it later
    Bail(T, U), // Bail out of the operation entirely.
}

impl<T, I: Iterator<Item = T>> ShrinkWrapper<T, I> {
    pub fn new(iter: I) -> Self {
        Self { iter }
    }

    pub fn shrink<U>(
        self,
        mut acc: U,
        shrink_fn: &mut impl FnMut(T, U) -> ShrinkStep<T, U>,
    ) -> Result<U, (U, VecDeque<T>)> {
        let Self { mut iter } = self;

        let mut to_revisit = VecDeque::with_capacity({
            let (lower, upper) = iter.size_hint();
            upper.unwrap_or(lower)
        });

        // First, drain values from the iterator, eagerly accumulating them if
        // possible, and putting the others into the accumulator.
        while let Some(next) = iter.next() {
            acc = match shrink_fn(next, acc) {
                ShrinkStep::Take(u) => u,
                ShrinkStep::Pass(t, u) => {
                    to_revisit.push_back(t);
                    u
                }
                ShrinkStep::Bail(t, u) => {
                    to_revisit.push_back(t);
                    return Err((u, to_revisit));
                }
            };
        }

        let mut checked_vals = 0;
        while let Some(next) = to_revisit.pop_front() {
            acc = match shrink_fn(next, acc) {
                ShrinkStep::Take(u) => {
                    // Successfully got a value, reset the check counter.
                    checked_vals = 0;
                    u
                }
                ShrinkStep::Pass(t, u) => {
                    // Didn't use the value, put it back and increment the
                    // counter.
                    to_revisit.push_back(t);
                    checked_vals += 1;
                    u
                }
                ShrinkStep::Bail(t, u) => {
                    // We don't really care what the counter's doing if  we're
                    // bailing out.
                    to_revisit.push_back(t);
                    return Err((u, to_revisit));
                }
            };

            if checked_vals >= to_revisit.len() && !to_revisit.is_empty() {
                return Err((acc, to_revisit));
            }
        }
        Ok(acc)
    }
}

impl<T, I: IntoIterator<Item = T>> From<I> for ShrinkWrapper<T, I::IntoIter> {
    fn from(value: I) -> Self {
        Self {
            iter: value.into_iter(),
        }
    }
}
